//Librerias instaladas
const app = require('express')();
const mongoose = require('mongoose');
//Librerias locales
const { MONGO_URI } = require('./src/config/index');
//const BOT = require ('./src/telegram')

//Conectarnos a nuestras base de datos
mongoose.connect(MONGO_URI, { useNewUrlParser:true, useUnifiedTopology:true }).then (p=>{
    console.log('Conexion exitosa');
}).catch(err=>{
    console.log(err);
});


//Puertoo
app.listen(5000);
app.get("/", (req, res)=>{
    res.send("");
})

//Telegram
require('./src/telegram');
