const mongoose = require('mongoose');

const {Schema}=mongoose

const usuarioSchema = new Schema(
    {
        idusuario:{type:String},
        iduser:{type:String},
        nomApe: {type:String},
        date:{type:Date}
    },
    {
        timestamps:true
    }
)

const Usuario = mongoose.model("usuario", usuarioSchema)
module.exports = Usuario;